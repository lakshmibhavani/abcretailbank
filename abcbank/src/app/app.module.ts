import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { CreateAccountComponent } from './components/create-account/create-account.component';
import { UserService } from './services/user.service';
import { TransactionComponent } from './components/transaction/transaction.component';
import { SearchComponent } from './components/search/search.component';
import { AccountService } from './services/account.service';
import { AccountSummaryComponent } from './components/account-summary/account-summary.component';

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    CreateAccountComponent,
    TransactionComponent,
    SearchComponent,
    AccountSummaryComponent,
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [UserService,AccountService],
  bootstrap: [AppComponent]
})
export class AppModule { }
