export class Account {
    id:number;
    userId:number;
    accountHolderName:string;
    branchname:string;
    balance:number;
    accountNumber:number;
}
