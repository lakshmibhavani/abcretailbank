import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { Account } from 'src/app/models/account';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/models/user';
import { AccountService } from 'src/app/services/account.service';

@Component({
  selector: 'app-create-account',
  templateUrl: './create-account.component.html',
  styleUrls: ['./create-account.component.css']
})
export class CreateAccountComponent implements OnInit {

  account:Account=new Account();
  users:any;
  
  constructor(private router:Router,private accountService:AccountService,private userService:UserService) { }

  
  ngOnInit(): void {
   this.users= this.userService.getUserdetails().subscribe(data=>console.log(data));
  }
  onSubmit(userForm){
    if(userForm.form.valid){
   this.saveAccount();
    }
  }
  saveAccount(){
    this.accountService.saveAccount(this.account).subscribe(data=>console.log(data));
  }
  
}




