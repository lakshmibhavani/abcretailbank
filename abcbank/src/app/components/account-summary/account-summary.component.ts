import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-account-summary',
  templateUrl: './account-summary.component.html',
  styleUrls: ['./account-summary.component.css']
})
export class AccountSummaryComponent implements OnInit {

trasactions:any=null;
items = ["customerId", "date"];
customerId='';
startDate = ''
endDate = '';
selectedItem='';
  constructor(private userService :UserService) { }

  ngOnInit(): void {
  }
  selected(){

  }
  
  getAccoumtSummary(customerId) {
    console.log("--------------------" + customerId)

    this.trasactions = [];
    this.userService.getTransactionsById(customerId).subscribe(res => {
      res.forEach(t => { this.trasactions.push(t); });


    });
  }
 

  getBydate(startDate, endDate) {

    console.log(startDate)
    var std = new Date(startDate);
    var etd = new Date(endDate);

    this.trasactions = [];

    this.userService.getBydate(startDate, endDate).subscribe(res => {
      console.log(res);
      res.forEach(t => { this.trasactions.push(t); });

    })





  }


}
