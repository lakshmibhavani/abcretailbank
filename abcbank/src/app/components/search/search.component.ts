import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/services/user.service';
import { AccountService } from 'src/app/services/account.service';
import { Account } from 'src/app/models/account';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {
  accounts:any;
  account:Observable<Account>;
  accountNumber:any;
  ngOnInit(): void {
    this.accountService.getAllAccounts().subscribe((data)=>{this.accounts=data;});
  }
constructor(public accountService:AccountService){

}

Search(accountNum){
  if(accountNum==""){
    this.ngOnInit();
  }else{
    this.account=this.accountService.getByAccountNumber(accountNum);
    console.log(this.account);
  }
}

}
