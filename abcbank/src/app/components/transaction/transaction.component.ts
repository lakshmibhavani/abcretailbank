import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/models/user';
import { UserService } from 'src/app/services/user.service';
import { Transactions } from 'src/app/models/transactions';
import { Account } from 'src/app/models/account';
import { AccountService } from 'src/app/services/account.service';

@Component({
  selector: 'app-transaction',
  templateUrl: './transaction.component.html',
  styleUrls: ['./transaction.component.css']
})
export class TransactionComponent implements OnInit {

  fromAccountNumber;
  toAccountNumber;
  ammount;
  constructor(private userService: UserService,private accountService :AccountService) { }

  ngOnInit(): void {
  }
  transaction(fromAccountNumber, toAccountNumber, ammount) {
    let fromAcc = new Account();
    let toAcc = new Account();
    console.log(ammount)


    this.accountService.getByAccountNumber(fromAccountNumber).subscribe(res => {
      fromAcc = res[0]
      console.log(fromAcc.balance)
      
      if (fromAcc.balance > ammount && ammount > 0) {

        fromAcc.balance = fromAcc.balance - ammount
        console.log(fromAcc.balance)
        this.accountService.updateAccount(fromAcc.id, fromAcc).subscribe(data=>{console.log(data)})
        console.log(fromAcc.balance);

        this.accountService.getByAccountNumber(toAccountNumber).subscribe(res => {
          toAcc = res[0]
          console.log(toAcc.accountNumber)
          console.log(toAcc.balance)
          toAcc.balance = toAcc.balance+ ammount
          console.log(toAcc.balance)
          this.accountService.updateAccount(toAcc.id, toAcc).subscribe()
          console.log(toAcc);
          this.userService.getTrans().subscribe(result => {
            console.log(result.length)



            let trans = new Transactions();
            trans.ammount = ammount;
            trans.balance = fromAcc.balance;
            trans.date = new Date();
            trans.customerId = fromAcc.id;
            trans.toAccountNumber = toAcc.accountNumber;

            trans.id = result.length + 1;
            trans.tId = result.length + 1;

            this.userService.saveTransactionsDetails(trans).subscribe(res => {
              console.log(JSON.stringify(res));

            });

          });
        });


        alert("transaction is successfull");




      } else {

        alert("please enter correct details")
      }


    });


  }

}
