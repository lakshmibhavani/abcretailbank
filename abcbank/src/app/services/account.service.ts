import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Account } from 'src/app/models/account';
@Injectable({
  providedIn: 'root'
})
export class AccountService {
  
  

  private baseUrl='http://localhost:8081/account';

  constructor(private http:HttpClient) { }
  
  saveAccount(account :Account) : Observable<any>
  {
      
      return this.http.post(`${this.baseUrl}`,account);
  }
  updateAccount(id:number,account:Account):Observable<Object>{
    return this.http.put(`${this.baseUrl}/${id}`,account);
  }
  getByAccountNumber(accountNumber){
     
    return this.http.get<Account>(this.baseUrl+"?accountNumber="+accountNumber);   
}
getAllAccounts() :Observable<any>{
  return this.http.get(`${this.baseUrl}`); 
}
}
