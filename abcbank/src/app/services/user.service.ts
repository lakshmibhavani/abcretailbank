import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Account } from 'src/app/models/account';
import { User } from '../models/user';
import { Transactions } from '../models/transactions';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class UserService {
  
  private baseUrl='http://localhost:3000';

  constructor(private http:HttpClient) { }
  
  saveUserDetails(user : User) : Observable<any>
  {
      let url = this.baseUrl+'user';
      return this.http.post(url,user);
  }
  getUserdetails():Observable<any>{
    return this.http.get(`${this.baseUrl}`+"/user");
  }
  
  getUser(id:number):Observable<any>{
    return this.http.get<User>(this.baseUrl+"/user?customerId="+id);

  }
  updateUser(id,user):Observable<any>{
    return this.http.put(`${this.baseUrl}`+"+/user/"+id,user);
  }
 
saveTransactionsDetails(trans : Transactions) : Observable<any>
  {
      let url = "http://localhost:4000/transactions";
      return this.http.post(url,trans);
  }
  

  getTrans(){
    let url = "http://localhost:4000/transactions";

      return this.http.get(url)
        .pipe(
          map(responseData => {
           //console.log(responseData);
           const transactions = [];
           for (const key in responseData) {
 
             console.log(responseData[key].date+"keyvalue")
             if (responseData.hasOwnProperty(key)) {
 
 
              transactions.push({ ...responseData[key], id: key });
             }
           }
           return transactions;
         })
       
       ) 
  }
  getBydate(startDate:Date,endDate:Date){

   
    
    let  p=new Date();
   
        let url = "http://localhost:4000/transactions";
    
        return this.http
         .get(url)
         .pipe(
           map(responseData =>
            {
             const transactions = [];
             for (const key in responseData) {
               let a=responseData[key].date;
               let k=new Date(a);
            
               if (k>=startDate&&k<=endDate) {
                transactions.push({ ...responseData[key], id: key });
               } 
             }
             console.log(transactions)
             return transactions;
           })
         
         )
    
    
    
      }
    
  getTransactionsById(customerId:number){
        let url = "http://localhost:4000/transactions";
    
    
    return this.http.get(url+"?customerId="+customerId) .pipe(
      map(responseData => {
        console.log(responseData);
        const transactions = [];
        for (const key in responseData) {
    
          //console.log(key+"keyvalue")
          if (responseData.hasOwnProperty(key)) {
    
    
           transactions.push({ ...responseData[key], id: key });
          }
        }
        return transactions;
      })
    
    );
    }    
    getTransactionsByAccountNumber(accountNumber: number) {
      let url = "http://localhost:4000/transactions";
      return this.http.get(url+"?accountNumber="+accountNumber) .pipe(
        map(responseData => {
          console.log(responseData);
          const transactions = [];
          for (const key in responseData) {
      
            //console.log(key+"keyvalue")
            if (responseData.hasOwnProperty(key)) {
      
      
             transactions.push({ ...responseData[key], id: key });
            }
          }
          return transactions;
        })
      
      );
    }
}